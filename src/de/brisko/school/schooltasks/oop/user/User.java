package de.brisko.school.schooltasks.oop.user;

public class User {

    private String username;
    private String email;
    private String password;
    private String birthDate;
    private String address;
    private boolean isAdmin;
    private long lastLogin;

    public User() {
    }

    public User(String username, String email, String password, String birthDate, String address, boolean isAdmin, long lastLogin) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.address = address;
        this.isAdmin = isAdmin;
        this.lastLogin = lastLogin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(long lastLogin) {
        this.lastLogin = lastLogin;
    }

    public void introduce() {
        System.out.println("Hello! My username is " + username + " and my mail-address is " + email);
    }
}
