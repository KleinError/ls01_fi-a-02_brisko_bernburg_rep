package de.brisko.school.schooltasks.oop.user;

import java.time.LocalDate;

public class UserTest {

    private static final String username = "test";
    private static final String email = "mail@test.de";
    private static final String password = "passordxy";
    private static final String birthdate = LocalDate.now().toString();
    private static final String address = "TestAddress Street Nr. 5";
    private static final boolean isAdmin = false;
    private static final long lastLogin = System.currentTimeMillis();

    private static int errors = 0;

    public static void main(String[] args) {
        System.out.println("\nStart Testing User...\n");

        var user = new User();

        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        user.setBirthDate(birthdate);
        user.setAddress(address);
        user.setAdmin(isAdmin);
        user.setLastLogin(lastLogin);

        assertEqual(username, user.getUsername());
        assertEqual(email, user.getEmail());
        assertEqual(password, user.getPassword());
        assertEqual(birthdate, user.getBirthDate());
        assertEqual(address, user.getAddress());
        assertEqual(isAdmin, user.isAdmin());
        assertEqual(lastLogin, user.getLastLogin());

        System.out.println("Stop Testing User!");
        System.out.println("There are " + errors + " Errors!");
    }

    private static void assertEqual(Object expected, Object actual) {
        if (!expected.equals(actual)) {
            System.out.println(expected.toString() + " not equal to " + actual.toString());
            errors++;
        }
    }
}
