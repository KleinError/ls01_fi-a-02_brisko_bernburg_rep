package de.brisko.school.schooltasks.konsolenausgabe;

public class Temperature {

    public static void main(String[] args) {

        System.out.printf("%s | %s%n", "Fahrenheit", "Celsius");

        for(int i = 0; i <= 5; i++) {
            System.out.printf("%s | %s%n", i, getCelsius(i));
        }
    }

    private static double getCelsius(int fahrenheit) {
        return (fahrenheit * 1.8 + 32);
    }

}
