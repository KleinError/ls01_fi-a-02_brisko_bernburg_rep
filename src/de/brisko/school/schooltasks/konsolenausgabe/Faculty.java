package de.brisko.school.schooltasks.konsolenausgabe;

public class Faculty {

    public static void main(String[] args) {
        for (int counter = 0; counter <= 5; counter++){
            System.out.printf("%d! = %d\n", counter, factorial(counter));
        }
    }

    private static long factorial(long number) {
        if (number <= 1)
            return 1;
        else
            return number * factorial(number - 1);
    }

}
