package de.brisko.school.schooltasks.datatypes;

public class WeltDerZahlen {

    public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        var anzahlPlaneten = 8;

        // Anzahl der Sterne in unserer Milchstraße
        int anzahlSterne = 250;

        // Wie viele Einwohner hat Berlin?
        var bewohnerBerlin = 380;

        // Wie alt bist du?  Wie viele Tage sind das?

        var alterTage = 5982;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        var gewichtKilogramm = 53;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        var flaecheGroessteLand = 17.1;

        // Wie groß ist das kleinste Land der Erde?

        var flaecheKleinsteLand = 0.44;


    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

        System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
        System.out.println("Anzahl der Sterne: " + anzahlSterne + " Mrd.");
        System.out.println("Einwohner Berlin: " + bewohnerBerlin + " Mio.");
        System.out.println("Mein Alter in Tage: " + alterTage);
        System.out.println("Mein Gewicht: " + gewichtKilogramm + " kg");
        System.out.println("Fläche größtes Land (Russland): " + flaecheGroessteLand + " Mio km²");
        System.out.println("Fläche kleinstes Land (Staat Vatikanstadt): " + flaecheKleinsteLand + " km²");


        System.out.println(" *******  Ende des Programms  ******* ");

    }
}
