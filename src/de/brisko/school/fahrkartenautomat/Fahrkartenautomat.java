package de.brisko.school.fahrkartenautomat;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Fahrkartenautomat {

    private static int tickets;
    private static int ticketType;

    private static double zuZahlenderBetrag;
    private static double eingezahlterGesamtbetrag = 0.0;
    private static double eingeworfeneMuenze;
    private static double rueckgabebetrag;

    private static String[] ticketTypeNames = new String[10];
    private static Double[] ticketTypePrices = new Double[10];

    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);

        setTicketTypeNames();
        setTicketTypePrices();

        requestTicketType(tastatur);
        requestTicketCount(tastatur);
        zuZahlenderBetrag = calculatePrice(ticketTypePrices[ticketType - 1], tickets);

        requestMoney(tastatur);
        ticketOutput();
        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        moneyBack();

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir wünschen Ihnen eine gute Fahrt.");
    }

    private static double calculatePrice(double ticketPrice, int count) {
        return ticketPrice * count;
    }

    private static void requestTicketCount(Scanner tastatur) {
        System.out.print("Anzahl: ");
        final int inputTickets = tastatur.nextInt();

        if (inputTickets > 0 && inputTickets <= 10) {
            tickets = inputTickets;
        } else {
            System.out.println("\nUngültige Anzahl! (mind. 1, max. 10) \n");
            requestTicketCount(tastatur);
        }
    }

    public static void requestTicketType(Scanner scanner) {
        System.out.println("Bitte wähle eine Fahrkarte:");

        for (int i = 1; i < ticketTypeNames.length + 1; i++) {
            int index = i - 1;
            System.out.println(i + "| " + ticketTypeNames[index] + " [" + new DecimalFormat("#0.00").format(ticketTypePrices[index]) + "€]");
        }

        System.out.print("Fahrkarte: ");
        final int type = scanner.nextInt();

        if (type >= 1 && type <= ticketTypeNames.length) {
            ticketType = type;
        } else {
            System.out.println("\nUngültige Fahrkarte!\n");
            requestTicketType(scanner);
        }
    }

    private static void requestMoney(Scanner tastatur) {
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.print("Noch zu zahlen: ");
            System.out.printf("%.2f%s%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
    }

    private static void ticketOutput() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    private static void moneyBack() {
        if (rueckgabebetrag > 0.0) {
            System.out.print("Der rueckgabebetrag in Höhe von ");
            System.out.printf("%.2f%s%n", rueckgabebetrag, " Euro ");
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {
                System.out.println("2 Euro");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {
                System.out.println("1 Euro");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) {
                System.out.println("50 Cent");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) {
                System.out.println("20 Cent");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) {
                System.out.println("10 Cent");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05) {
                System.out.println("5 Cent");
                rueckgabebetrag -= 0.05;
            }
        }
    }

    private static void setTicketTypeNames() {
        ticketTypeNames[0] = "Einzelfahrschein Berlin AB";
        ticketTypeNames[1] = "Einzelfahrschein Berlin BC";
        ticketTypeNames[2] = "Einzelfahrschein Berlin ABC";
        ticketTypeNames[3] = "Kurzstrecke";
        ticketTypeNames[4] = "Tageskarte Berlin AB";
        ticketTypeNames[5] = "Tageskarte Berlin BC";
        ticketTypeNames[6] = "Tageskarte Berlin ABC";
        ticketTypeNames[7] = "Kleingruppen-Tageskarte Berlin AB";
        ticketTypeNames[8] = "Kleingruppen-Tageskarte Berlin BC";
        ticketTypeNames[9] = "Kleingruppen-Tageskarte Berlin ABC";
    }

    private static void setTicketTypePrices() {
        ticketTypePrices[0] = 2.9;
        ticketTypePrices[1] = 3.3;
        ticketTypePrices[2] = 3.6;
        ticketTypePrices[3] = 1.9;
        ticketTypePrices[4] = 8.6;
        ticketTypePrices[5] = 9.0;
        ticketTypePrices[6] = 9.6;
        ticketTypePrices[7] = 23.5;
        ticketTypePrices[8] = 24.3;
        ticketTypePrices[9] = 24.0;
    }

}